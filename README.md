# Recruitment Assignment
Classify people to predict the category of income and determine potential donors.

### Objective
Prepare a census dataset, analyze trends, and train a few ML algorithms on a subset of it. Evaluate the quality of predictions.

Link to the dataset:
- https://archive.ics.uci.edu/ml/datasets/adult

### Steps
I divided the analysis in three parts:

  * `Data Preparation:` cleaning, feature reduction, handling of missing values, encoding [My Jupyter Notebook for Data Prep][prep]
  * `Analysis:` correlation distributions, importance of features [My Jupyter Notebook for Data Analysis][analysis]
  * `Prediction:` test of various categorisation algorithms, their efficiency and possibility of reducing the number of features [My Jupyter Notebook for Predictions][pred]
  
  
[prep]:https://gitlab.com/achuinar/keela/blob/master/DataPrep_Keela.ipynb   
[analysis]:https://gitlab.com/achuinar/keela/blob/master/DataAnalysis_Keela.ipynb
[pred]:https://gitlab.com/achuinar/keela/blob/master/Predictions_Keela.ipynb
  